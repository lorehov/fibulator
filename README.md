Fibulator
=========

From fibonacci calculator.

How to launch:
--------------

Fibulator doesn't depends on any third party libraries, so following is enough:

```
go build
./fibulator [--port [--debug]]
```

or just

```
go run main.go
```

Now fibulator available on specified port:

`http://localhost:8000/?n=10`

Tests
-----

```
go test
```
