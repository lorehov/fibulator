package main

import (
	"math/big"
	"net/http"
	"strconv"
	"errors"
	"log"
	"flag"
	"fmt"
)


var portFlag = flag.String("port", "8000", "port to serve")
var debug = flag.Bool("debug", false, "enable debug output")


func fib(n int) ([]*big.Int, error) {
	series := []*big.Int{big.NewInt(0), big.NewInt(1)}

	if n < 0 {
		return nil, errors.New("`n` should be positive number")
	}
	if n == 0 {
		return series[0:1], nil
	}
	if n == 1 {
		return series[0:2], nil
	}

	for i := 2; i <= n; i ++ {
		val := big.NewInt(0)
		val = val.Add(series[i-1], series[i-2])
		series = append(series, val)
	}
	return series, nil
}


func onError(w http.ResponseWriter, code int, msg string) {
	log.Printf(msg)
	http.Error(w, msg, code)
}


func Handler(w http.ResponseWriter, r *http.Request) {
	rawN := r.URL.Query().Get("n")
	if *debug {
		log.Printf("`n` %v received", rawN)
	}

	if rawN == "" {
		onError(w, http.StatusBadRequest, "`n` parameter not specified")
		return
	}

	N, err := strconv.ParseInt(rawN, 10, 32)
	if err != nil {
		onError(w, http.StatusBadRequest,
			fmt.Sprintf("Can't convert `n` '%v' to int64 with error '%v'",
			rawN, err))
		return
	}

	series, err := fib(int(N))
	if err != nil {
		onError(w, http.StatusBadRequest,
			fmt.Sprintf("Can't calculate fibonacci series for '%v', '%v'",
			N, err))
		return

	}

	slen := len(series)
	if *debug {
		log.Printf("series for %v is %v", rawN, series)
	}

	w.WriteHeader(http.StatusOK)
	for i := range series {
		fmt.Fprintf(w, series[i].String())
		if i < slen - 1 {
			fmt.Fprintf(w, " ")
		}
	}
}


func main() {
	flag.Parse()
	port := ":" + *portFlag
	http.HandleFunc("/", Handler)
	log.Printf("Start listening on port %s", port)
	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatalf("Failed to start serving on port %s with error: %s",
			port, err)
	}
}
