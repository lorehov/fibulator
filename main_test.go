package main

import (
	"testing"
	"net/http/httptest"
	"net/http"
	"strings"
)


type testCase struct {
	N string
	Body string
	Code int
}


func TestHandler(t *testing.T) {
	testCases := []testCase{
		{"", "`n` parameter not specified", 400},
		{"asd", "Can't convert `n` 'asd' to int64 with error 'strconv.ParseInt: parsing \"asd\": invalid syntax'", 400},
		{"-10", "Can't calculate fibonacci series for '-10', '`n` should be positive number'", 400},
		{"2147483648", "Can't convert `n` '2147483648' to int64 with error 'strconv.ParseInt: parsing \"2147483648\": value out of range'", 400},
		{"0", "0", 200},
		{"1", "0 1", 200},
		{"2", "0 1 1", 200},
		{"3", "0 1 1 2", 200},
		{"10", "0 1 1 2 3 5 8 13 21 34 55", 200}}

	for _, tc := range testCases {
		req, err := http.NewRequest("GET", "http://localhost:8000/?n=" + tc.N, nil)
		if err != nil {
			t.Fatal(err)
		}
		w := httptest.NewRecorder()
		Handler(w, req)
		if w.Code != tc.Code {
			t.Errorf(
				"Wrong response code for N %s: '%s', but should be '%s'",
				tc.N, w.Code, tc.Code)
		}
		body := strings.Trim(w.Body.String(), "\n")
		if body != tc.Body {
			t.Errorf(
				"Wrong response body for N %s: '%s', but should be '%s'",
				tc.N, body, tc.Body)
		}
	}
}

